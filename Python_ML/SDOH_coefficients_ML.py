import pandas as pd
import numpy as np
from numpy import inf
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.linear_model import Lasso

filename = './Data/icd_sparse_v2_no51K.csv'
df = pd.read_csv(filename)

# Remove any null or NA vlaues
df = df[pd.notnull(df['svc_tot_cost'])]
df.fillna(value=0, inplace=True)

# Apply log to costs
df['svc_tot_cost'] = df['svc_tot_cost'].apply(lambda x:np.log10(x))

# Remove patient identifier so df only contains independent variables
del df['mbr_id_alt']
#del df['mbr_dk']

# Isolate dependent cost variable
y = df['svc_tot_cost']
del df['svc_tot_cost']

# Split training & test datasets
X = df
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Feeding data to SVR
X_train = X_train.as_matrix()
X_test = X_test.as_matrix()
y_train = y_train.as_matrix()
y_test = y_test.as_matrix()

# SVR model
model = SVR(kernel='linear', C=10, epsilon=0.2)

# Lasso model
#model = Lasso(alpha=0.1)

# Run model
clf = model.fit(X_train, y_train)
y_pred = clf.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)
print(r2)

# Analysis
coeff = clf.coef_
out = coeff.tolist()

# Output (comment out if using Lasso
out = [item for sublist in out for item in sublist]
out = pd.DataFrame(out)
print(out)

# SVR output
out.to_csv(('./Output/coefficients_SVR_ICD_v2_no51k.csv'), header=False, index=False)

#  Lasso output
#out.to_csv(('Lasso_coefficients_' + filename), header=False, index=False)