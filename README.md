# SDOH_ML

Contains R and Python files used to conduct machine learning on the SDOH dataset.

Currently R used to clean the data, Python used to perform ML.

No PHI included in this repo - /Data & /Output files excluded. Contact Tristan Kaiser for access to these files. 